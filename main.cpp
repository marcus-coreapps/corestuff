/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QMessageBox>
#include <QProcess>

#include <cprime/capplication.h>

#include "corestuff.h"
#include "button.h"

#include "DesQEventFilter.hpp"
#include "DesQWMSession.hpp"

int main( int argc, char *argv[] )
{
    CPrime::CApplication app( "CoreStuff", argc, argv);
	qApp->installNativeEventFilter( new DesQEventFilter( DesQWMSession::currentSession() ) );

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CoreStuff");
    app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("cc.cubocore.CoreStuff.desktop");
    app.setQuitOnLastWindowClosed(false);

    if ( app.isRunning() ) {
		if ( ( argc == 2 ) and ( argv[ 1 ] == QString( "--reload" ) ) ) {
			app.sendMessage( "reload" );
			return 0;
		}

		else {
			qDebug() << "Found a running instance. Exiting";
			QMessageBox::information(
				nullptr,
				"CoreStuff",
				"Another instance of CoreStuff is already open. Please close it before opening a new instance."
			);

			return 0;
		}
    }

	qDebug() << "KGlobal Started: " << QProcess::startDetached("kglobalaccel5", QStringList());


	DockButton w;
	w.show();

    return app.exec();
}
