/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>

#include "settings.h"

class QToolButton;
class QTreeWidgetItem;

namespace Ui {
class pagesessions;
}

class pagesessions : public QWidget
{
    Q_OBJECT

public:
    explicit pagesessions(QWidget *parent = nullptr);
    ~pagesessions();

    void reload();
    void addControls(QList<QToolButton *> controls);

public slots:
    void editSessoion();
    void deleteSession();
    void addSession();

private slots:
    void openSelectedSession(QTreeWidgetItem *item, int column);
    void on_sessionsView_itemSelectionChanged();

private:
    Ui::pagesessions *ui;
    QSize            listViewIconSize;
    int              uiMode;
    bool             useSystemNotification;
    settings        *smi;

    QToolButton *menu = nullptr;
    QToolButton *m_addSession = nullptr;
    QToolButton *m_editSession = nullptr;
    QToolButton *m_deleteSession = nullptr;

    void loadSettings();
    void setupSessionsView();
    void startSetup();
    void showSessionMessage();
    void menuClicked();
};

