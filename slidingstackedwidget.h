/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QObject>
#include <QStackedWidget>

class slidingStackedWidget : public QStackedWidget
{
    Q_OBJECT
public:
    enum SlidingDirection {
        LEFTTORIGHT,
        RIGHTTOLEFT
    };

    slidingStackedWidget(QWidget *parent);
    ~slidingStackedWidget();

    void slideNext();
    void slidePrevious();
    void slideInto(int index, SlidingDirection direction);

signals:
    void animationFinished();

private:
    int m_next;
    bool m_active;
    QPoint m_currPoint;

    void slideWidget(int index, QWidget *wid, SlidingDirection direction);
    void animationDone();
};
