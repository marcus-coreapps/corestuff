/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "pageapps.h"
#include "ui_pageapps.h"

#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QGraphicsDropShadowEffect>
#include <QtEvents>
#include <QScrollBar>
#include <QScroller>

#include <cprime/systemxdg.h>
#include <cprime/applicationdialog.h>
#include <cprime/appopenfunc.h>
#include <cprime/themefunc.h>

QMutex appLock;

pageapps::pageapps(QWidget *parent) :QWidget(parent)
  , ui(new Ui::pageapps)
  , smi(new settings)
{
    ui->setupUi(this);
    loadSettings();
    startSetup();
    setupAppsView();
}

pageapps::~pageapps()
{
	delete smi;
    delete ui;
}

/**
 * @brief Loads application settings
 */
void pageapps::loadSettings()
{
    iconViewIconSize = smi->getValue("CoreApps", "IconViewIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
}

void pageapps::startSetup()
{
    ui->appsView->setIconSize( iconViewIconSize );
    ui->appsView->setGridSize( QSize( iconViewIconSize.width() * 3, iconViewIconSize.height() * 2 ) );

    QFileSystemWatcher *watcher = new QFileSystemWatcher();
    watcher->addPath( "/usr/share/applications/" );

    connect( watcher, &QFileSystemWatcher::directoryChanged, this, &pageapps::setupAppsView );

    if ( uiMode != 0 ) {
        QScroller::grabGesture(ui->appsView, QScroller::LeftMouseButtonGesture);
    }
}

void pageapps::setupAppsView()
{
    ui->appsView->clear();
	ui->appsView->setSizeAdjustPolicy( QListWidget::AdjustToContents );

	// QtConcurrent::run( this, &pageapps::loadApps );
}

void pageapps::on_appsView_itemClicked(QListWidgetItem *item)
{
    CPrime::DesktopFile df = qvariant_cast<CPrime::DesktopFile>( item->data( Qt::UserRole + 1 ) );
    if ( df.isValid() )
        df.startApplicationWithArgs( QStringList() );
}

void pageapps::on_appsView_itemActivated(QListWidgetItem *item)
{
	CPrime::DesktopFile df = qvariant_cast<CPrime::DesktopFile>( item->data( Qt::UserRole + 1 ) );
	if ( df.isValid() )
		df.startApplicationWithArgs( QStringList() );
}

void pageapps::loadApps() {

	appLock.lock();
    CPrime::SystemXdgMime::instance()->parseDesktops();
    CPrime::AppsList list = CPrime::SystemXdgMime::instance()->allDesktops();

    std::sort( list.begin(), list.end(), [ ]( const CPrime::DesktopFile& lhs, const CPrime::DesktopFile& rhs ) {
        return lhs.name() < rhs.name();
    } );

    for (int i = 0; i < list.count(); i++) {
        CPrime::DesktopFile df = list[i];

        if ( not df.isValid() )
            continue;

        if ( not df.visible() )
            continue;

        QIcon icon = CPrime::ThemeFunc::getAppIcon( df.desktopName() );
        icon = CPrime::ThemeFunc::resizeIcon( icon, ui->appsView->iconSize() );

        QListWidgetItem *item = new QListWidgetItem( icon, df.name() );
        item->setData( Qt::UserRole + 1, QVariant::fromValue<CPrime::DesktopFile>( df ) );
        item->setSizeHint( QSize( 110, 100 ) );
        ui->appsView->addItem( item );
    }

	appLock.unlock();
}
