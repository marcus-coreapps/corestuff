/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef CORESTUFF_H
#define CORESTUFF_H

#include <QWidget>
#include <QToolButton>
#include <QFileSystemWatcher>

#include "settings.h"
#include "gesturehandler.h"


class pageactivites;
class pagehome;
class pagepins;
class pagereminders;
class pagesessions;
class pageapps;
class pagetasks;

namespace Ui {
    class corestuff;
}

class corestuff : public QWidget
{
    Q_OBJECT

public:
    explicit corestuff(QWidget *parent = nullptr);
    ~corestuff();

    void reload( const QString &path );

    void showTasksPage();
    void showDesktopT();
    void VolumeUp();
    void VolumeDown();
    void TakeCoreshot();

private slots:
    void on_home_clicked();
    void on_tasks_clicked();
    void on_activites_clicked();
    void on_sessions_clicked();
    void on_pins_clicked();
    void on_menu_clicked();
    void on_apps_clicked();

    void on_power_clicked();
    void on_clearActivites_clicked();
    void on_editSessoion_clicked();
    void on_deleteSession_clicked();
    void on_addSession_clicked();

    void handleMessages( const QString );
    void previousPage();
    void nextPage();

private:
    Ui::corestuff *ui;
    QFileSystemWatcher *fswStart;
    int currentPage = 0;
    bool showActivities;
    int uiMode;
    settings *smi;
    QPixmap m_bgpix;
    QSize toolsIconSize;
    QImage bgImage;
    int wallpaperPos;
    int pixOffsetX = 0;
    int pixOffsetY = 0;

    pageactivites *pActivities = nullptr;
    pagehome *pHome = nullptr;
    pagepins *pPins = nullptr;
    pagereminders *pReminders = nullptr;
    pagesessions *pSessions = nullptr;
    pageapps *pApps = nullptr;
    pagetasks *pTasks = nullptr;
    GestureHandler *gesture;

    void pageClick( QToolButton *btn, int i, QString windowTitle );
    void loadSettings();
    void startSetup();
    void setupIcons();
    void shortcuts();

    void updateBGPix();
    void resizeWindow();

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void leaveEvent(QEvent *event) override;
    bool eventFilter(QObject *obj, QEvent *event) override;
    void closeEvent(QCloseEvent *event) override;
    void paintEvent(QPaintEvent *pEvent) override;


Q_SIGNALS:
    void activateDock();
};

#endif // CORESTUFF_H
