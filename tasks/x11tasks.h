/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QListWidget>

#include "DesQClient.hpp"
#include "DesQEventFilter.hpp"
#include "DesQWMSession.hpp"

#include <settings.h>

#include <cprime/variables.h>


class TaskItem : public QWidget {
    Q_OBJECT

public:
    explicit TaskItem(QString title, QIcon icon, quint64 winID, QListWidgetItem *container, QWidget *parent = nullptr);
    ~TaskItem() {}

    QString getTitle();
    QIcon getIcon();
    quint64 getWinID();
    QListWidgetItem *getContainer();

signals:
    void closeOccured(QListWidgetItem *);

private:
    QIcon m_icon;
    QString m_title;
    quint64 m_winID;
    QListWidgetItem *m_container;
    settings *smi;
};

class X11Tasks : public QObject {
    Q_OBJECT

    public:
		/* X11 Tasks instance */
		static X11Tasks* session();

        /* Shutting the plugin */
        ~X11Tasks();

        /* Target of the plugin */
        void setBackendTarget( QListWidget * );

        /* Update the list of clients */
        void updateClientList();

        /* Show desktop */
        void showDesktop();

        /* Show/unshow the desktop */
        QSize desktopSize();

    private:
        QListWidget *target = nullptr;
        DesQWMSession *mSession = nullptr;
        Windows oldList;
        settings *smi;

        int uiMode;
        bool mShowingDesktop = false;

        QHash<Window, bool> wLists;
        Window mActiveWinID;
		QMap<Window, int> mWindowStates;

		static bool mInit;
		static X11Tasks *mTasks;

		/* Init the session */
        X11Tasks();

    public Q_SLOTS:
        void updateActiveWindow();
        void activateWindow( QListWidgetItem * );
        void closeActiveApp( QListWidgetItem *item );

    Q_SIGNALS:
        /* Signal for corestuff to resize to occupy full available space */
        void resizeDesktop();

        /* Signal for corestuff to activate the dock button */
        void activateDock();
};
