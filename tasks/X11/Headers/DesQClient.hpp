/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <DesQGlobals.hpp>

class DesQClientPrivate;

class DesQClient : public QObject {
	Q_OBJECT

	public:
		enum DesQClientType {
			None					= 0x000000,
			Desktop					= 0x11D8E4,
			Dock,
			Toolbar,
			Menu,
			Utility,
			Splash,
			Dialog,
			Dropdown_menu,
			Popup_menu,
			Tooltip,
			Notification,
			Combo,
			Dnd,
			Normal
		};

		DesQClient( Window wid );
		DesQClient( const DesQClient& );

		qulong type() const;
		qulong pid() const;
		qulong wid() const;
		int desktop() const;
		QString title() const;

		QIcon icon() const;
		QString typeStr() const;

		QImage screengrab() const;

		bool isMinimized() const;
		bool isMaximized() const;
		bool isActiveWindow() const;

	public Q_SLOTS:
		void activate();
		void minimize();
		void restore();
        void maximize();
		void close();

		void moveToDesktop( int );

	protected:
		DesQClientPrivate* d;
};
