/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <DesQGlobals.hpp>
#include <DesQClient.hpp>
#include <X11Helpers.hpp>

class DesQClientPrivate {

	public:
		DesQClientPrivate( DesQClient *dc );

		qulong mType;					// Normal window
		qulong mWid;					// Window ID
		uint32_t mPid;					// Process ID
		uint32_t mDesktop;				// Desktop Number
		QString mTitle;					// Window Title
		QIcon mIcon;					// Window Icon

		xcb_connection_t *XCB;			// XCB Connection
		xcb_ewmh_connection_t EWMH;		// XCB EWMH Connection
		bool mEWMHValid = false;		// Do we have a valid EWMH Connection?

	protected:
		DesQClient* q;
};
